{
    'name': 'Raloy Sales Order',
    'version': '1.0',
    'author': 'René Villegas',
    'depends': [
        'raloy_product_pieces_fields',
        'sale_stock'
    ],
    'data': [
        'views/sales_order.xml',
        'views/stock_picking.xml',
        'reports/sale_order_report.xml',
    ]
}
