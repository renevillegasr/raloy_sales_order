from odoo import api, fields, models, _

class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.depends('order_line.product_id.udv')
    def _udv_total(self):
        """
        Compute total of udv of the SO
        """
        for order in self:
            udv_total = 0.0
            for line in order.order_line:
                udv_total += line.product_uom_qty * line.product_tmpl_id.udv

            order.update({
                'udv_total': udv_total
            })

    udv_total = fields.Float(string='Total UDV', store=True, readonly=True, compute='_udv_total',
                             track_visibility='always')

    client_order_ref_2 = fields.Char(
        string='Referencia cliente 2',
    )

    routable_delivery = fields.Boolean(_('Bulk SO Route?'), default=False)
    scheduled_delivery = fields.Boolean(_('Scheduled SO'), default=False)
