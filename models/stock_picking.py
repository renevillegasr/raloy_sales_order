from odoo import models, fields, api, _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    routable_delivery_sale = fields.Boolean(related='sale_id.routable_delivery', store=True, string=_('Bulk SO Route?'))
    scheduled_delivery_sale = fields.Boolean(related='sale_id.scheduled_delivery', store=True, string=_('Scheduled SO'))
    related_company = fields.Many2one(
        string=_('Related Company'),
        store=True,
        related='partner_id.parent_id')
